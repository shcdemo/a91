<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A91782">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Upon the declaration of his Majesty King Charles of England the Second</title>
    <author>Richards, Nathaniel, 1611-1660.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A91782 of text R211920 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.25[28]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A91782</idno>
    <idno type="STC">Wing R1374</idno>
    <idno type="STC">Thomason 669.f.25[28]</idno>
    <idno type="STC">ESTC R211920</idno>
    <idno type="EEBO-CITATION">99870588</idno>
    <idno type="PROQUEST">99870588</idno>
    <idno type="VID">163833</idno>
    <idno type="PROQUESTGOID">2240938728</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A91782)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163833)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f25[28])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Upon the declaration of his Majesty King Charles of England the Second</title>
      <author>Richards, Nathaniel, 1611-1660.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for J.G.,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1660.</date>
     </publicationStmt>
     <notesStmt>
      <note>In verse - "Bless Mighty God great Britains second King".</note>
      <note>Annotation on Thomason copy: "May 18".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- II, -- King of England, 1630-1685 -- Poetry -- Early works to 1800.</term>
     <term>England and Wales. -- Sovereign (1660-1685 : Charles II) -- Poetry -- Early works to 1800.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685 -- Poetry -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A91782</ep:tcp>
    <ep:estc> R211920</ep:estc>
    <ep:stc> (Thomason 669.f.25[28]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Upon the declaration of his Majesty King Charles of England the Second.</ep:title>
    <ep:author>Richards, Nathaniel</ep:author>
    <ep:publicationYear>1660</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>287</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-07</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A91782-e10">
  <body xml:id="A91782-e20">
   <pb facs="tcp:163833:1" rend="simple:additions" xml:id="A91782-001-a"/>
   <div type="poem" xml:id="A91782-e30">
    <head type="illustration" xml:id="A91782-e40">
     <figure xml:id="A91782-e50">
      <p xml:id="A91782-e60">
       <w lemma="dieu" pos="ffr" reg="Dieu" xml:id="A91782-001-a-0010">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A91782-001-a-0020">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A91782-001-a-0030">MON</w>
       <w lemma="droit" pos="ffr" xml:id="A91782-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A91782-e70">
       <w lemma="honi" pos="ffr" xml:id="A91782-001-a-0050">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A91782-001-a-0060">SOIT</w>
       <w lemma="qvi" pos="ffr" reg="x" xml:id="A91782-001-a-0070">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A91782-001-a-0080">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A91782-001-a-0090">Y</w>
       <w lemma="pense" pos="ffr" reg="pennies" xml:id="A91782-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A91782-e80">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A91782-e90">
     <w lemma="upon" pos="acp" xml:id="A91782-001-a-0110">UPON</w>
     <w lemma="the" pos="d" xml:id="A91782-001-a-0120">THE</w>
     <w lemma="declaration" pos="n1" xml:id="A91782-001-a-0130">DECLARATION</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-0140">OF</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-0150">HIS</w>
     <w lemma="majesty" pos="n1" xml:id="A91782-001-a-0160">MAJESTY</w>
     <w lemma="king" pos="n1" xml:id="A91782-001-a-0170">KING</w>
     <w lemma="CHARLES" pos="nn1" xml:id="A91782-001-a-0180">CHARLES</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-0190">OF</w>
     <w lemma="ENGLAND" pos="nn1" xml:id="A91782-001-a-0200">ENGLAND</w>
     <w lemma="the" pos="d" xml:id="A91782-001-a-0210">the</w>
     <w lemma="second" pos="ord" xml:id="A91782-001-a-0220">Second</w>
     <pc unit="sentence" xml:id="A91782-001-a-0230">.</pc>
    </head>
    <l xml:id="A91782-e100">
     <w lemma="bless" pos="vvb" xml:id="A91782-001-a-0240">BLess</w>
     <w lemma="mighty" pos="j" xml:id="A91782-001-a-0250">Mighty</w>
     <w lemma="God" pos="nn1" xml:id="A91782-001-a-0260">God</w>
     <w lemma="great" pos="j" xml:id="A91782-001-a-0270">great</w>
     <w lemma="Britain" pos="nng1" reg="Britain's" xml:id="A91782-001-a-0280">Britains</w>
     <w lemma="second" pos="ord" xml:id="A91782-001-a-0290">second</w>
     <w lemma="king" pos="n1" xml:id="A91782-001-a-0300">KING</w>
    </l>
    <l xml:id="A91782-e110">
     <hi xml:id="A91782-e120">
      <w lemma="Charles" pos="nn1" xml:id="A91782-001-a-0310">Charles</w>
      <pc xml:id="A91782-001-a-0320">:</pc>
     </hi>
     <w lemma="shield" pos="vvb" xml:id="A91782-001-a-0330">shield</w>
     <w lemma="he" pos="pno" xml:id="A91782-001-a-0340">him</w>
     <w lemma="divinity" pos="n1" xml:id="A91782-001-a-0350">Divinity</w>
     <pc join="right" xml:id="A91782-001-a-0360">(</pc>
     <w lemma="from" pos="acp" xml:id="A91782-001-a-0370">from</w>
     <w lemma="the" pos="d" xml:id="A91782-001-a-0380">the</w>
     <w lemma="sting" pos="n1" xml:id="A91782-001-a-0390">Sting</w>
    </l>
    <l xml:id="A91782-e130">
     <w lemma="of" pos="acp" xml:id="A91782-001-a-0400">Of</w>
     <w lemma="black" pos="j" xml:id="A91782-001-a-0410">black</w>
     <w lemma="mouth" pos="vvn" reg="mouthed" xml:id="A91782-001-a-0420">mouth'd</w>
     <w lemma="murth'r" pos="vvg" xml:id="A91782-001-a-0430">Murth'ring</w>
     <w lemma="malice" pos="n1" xml:id="A91782-001-a-0440">Malice</w>
     <pc xml:id="A91782-001-a-0450">,</pc>
     <w lemma="make" pos="vvb" xml:id="A91782-001-a-0460">make</w>
     <w lemma="he" pos="pno" xml:id="A91782-001-a-0470">him</w>
     <w lemma="live" pos="vvb" xml:id="A91782-001-a-0480">Live</w>
    </l>
    <l xml:id="A91782-e140">
     <w lemma="the" pos="d" xml:id="A91782-001-a-0490">The</w>
     <w lemma="world" pos="ng1" reg="world's" xml:id="A91782-001-a-0500">worlds</w>
     <w lemma="true" pos="j" xml:id="A91782-001-a-0510">true</w>
     <w lemma="mirror" pos="n1" reg="mirror" xml:id="A91782-001-a-0520">Mirrour</w>
     <pc xml:id="A91782-001-a-0530">,</pc>
     <w lemma="that" pos="cs" xml:id="A91782-001-a-0540">that</w>
     <w lemma="do" pos="vvz" reg="does" xml:id="A91782-001-a-0550">do's</w>
     <w lemma="now" pos="av" xml:id="A91782-001-a-0560">now</w>
     <w lemma="forgive" pos="vvi" xml:id="A91782-001-a-0570">forgive</w>
    </l>
    <l xml:id="A91782-e150">
     <w lemma="free" pos="av-j" xml:id="A91782-001-a-0580">Freely</w>
     <w lemma="foul" pos="j" xml:id="A91782-001-a-0590">foul</w>
     <w lemma="fact" pos="n2" xml:id="A91782-001-a-0600">Facts</w>
     <pc xml:id="A91782-001-a-0610">;</pc>
     <w lemma="foul" pos="j" xml:id="A91782-001-a-0620">foul</w>
     <w lemma="fault" pos="n2" xml:id="A91782-001-a-0630">Faults</w>
     <pc xml:id="A91782-001-a-0640">,</pc>
     <w lemma="which" pos="crq" xml:id="A91782-001-a-0650">which</w>
     <w lemma="make" pos="vvz" xml:id="A91782-001-a-0660">makes</w>
     <w lemma="all" pos="d" xml:id="A91782-001-a-0670">all</w>
     <w lemma="those" pos="d" xml:id="A91782-001-a-0680">those</w>
    </l>
    <l xml:id="A91782-e160">
     <w lemma="enemy" pos="ng1" reg="Enemy's" xml:id="A91782-001-a-0690">Enemies</w>
     <w lemma="friend" pos="n2" xml:id="A91782-001-a-0700">Friends</w>
     <pc xml:id="A91782-001-a-0710">,</pc>
     <w lemma="that" pos="cs" xml:id="A91782-001-a-0720">that</w>
     <w lemma="be" pos="vvd" xml:id="A91782-001-a-0730">were</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-0740">his</w>
     <w lemma="great" pos="js" xml:id="A91782-001-a-0750">greatest</w>
     <w lemma="foe" pos="n2" xml:id="A91782-001-a-0760">Foes</w>
     <pc unit="sentence" xml:id="A91782-001-a-0770">.</pc>
    </l>
    <l xml:id="A91782-e170">
     <w lemma="king" pos="n1" xml:id="A91782-001-a-0780">KING</w>
     <hi xml:id="A91782-e180">
      <w lemma="Charles" pos="nn1" xml:id="A91782-001-a-0790">Charles</w>
     </hi>
     <w lemma="the" pos="d" xml:id="A91782-001-a-0800">the</w>
     <w lemma="first" pos="ord" xml:id="A91782-001-a-0810">First</w>
     <pc xml:id="A91782-001-a-0820">,</pc>
     <w lemma="that" pos="cs" xml:id="A91782-001-a-0830">that</w>
     <w lemma="glorious" pos="j" xml:id="A91782-001-a-0840">Glorious</w>
     <w lemma="martyr" pos="n1" xml:id="A91782-001-a-0850">Martyr</w>
     <pc xml:id="A91782-001-a-0860">,</pc>
     <w lemma="he" pos="pns" xml:id="A91782-001-a-0870">He</w>
    </l>
    <l xml:id="A91782-e190">
     <w lemma="of" pos="acp" xml:id="A91782-001-a-0880">Of</w>
     <w lemma="never-dying" pos="j" reg="neverdying" xml:id="A91782-001-a-0890">never-dying</w>
     <w lemma="bless" pos="j-vn" xml:id="A91782-001-a-0900">Blessed</w>
     <w lemma="memory" pos="n1" xml:id="A91782-001-a-0910">Memory</w>
     <pc xml:id="A91782-001-a-0920">,</pc>
    </l>
    <l xml:id="A91782-e200">
     <w lemma="his" pos="po" xml:id="A91782-001-a-0930">His</w>
     <w lemma="chief" pos="js" xml:id="A91782-001-a-0940">chiefest</w>
     <w lemma="charge" pos="n1" xml:id="A91782-001-a-0950">Charge</w>
     <w lemma="unto" pos="acp" xml:id="A91782-001-a-0960">unto</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-0970">his</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="A91782-001-a-0980">ROYALL</w>
     <w lemma="son" pos="n1" xml:id="A91782-001-a-0990">SON</w>
     <ref corresp="A91782-210-b" target="A91782-210-b" xml:id="A91782e-210-a"/>
     <note cert="high" corresp="A91782-210-a" place="margin" xml:id="A91782e-210-b" xml:lang="lat">
      <w lemma="expedit" pos="fla" xml:id="A91782-001-a-1000">Expedit</w>
      <w lemma="adversarios" pos="fla" xml:id="A91782-001-a-1010">adversarios</w>
      <w lemma="nostros" pos="fla" xml:id="A91782-001-a-1020">nostros</w>
      <w lemma="condonare" pos="fla" xml:id="A91782-001-a-1030">condonare</w>
      <pc xml:id="A91782-001-a-1040">,</pc>
      <w lemma="memoriamque" pos="fla" xml:id="A91782-001-a-1050">memoriamque</w>
      <w lemma="eorum" pos="fla" xml:id="A91782-001-a-1060">eorum</w>
      <w lemma="ex" pos="fla" xml:id="A91782-001-a-1070">ex</w>
      <w lemma="adversariis" pos="fla" xml:id="A91782-001-a-1080">adversariis</w>
      <w lemma="nostris" pos="fla" xml:id="A91782-001-a-1090">nostris</w>
      <w lemma="delere" pos="fla" xml:id="A91782-001-a-1100">delere</w>
      <pc unit="sentence" xml:id="A91782-001-a-1110">.</pc>
     </note>
    </l>
    <l xml:id="A91782-e220">
     <w lemma="be" pos="vvd" xml:id="A91782-001-a-1120">Was</w>
     <w lemma="to" pos="prt" xml:id="A91782-001-a-1130">to</w>
     <w lemma="forgive" pos="vvi" xml:id="A91782-001-a-1140">forgive</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-1150">his</w>
     <w lemma="enemy" pos="n2" xml:id="A91782-001-a-1160">Enemies</w>
     <pc xml:id="A91782-001-a-1170">;</pc>
     <w join="right" lemma="it" pos="pn" xml:id="A91782-001-a-1180">'t</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A91782-001-a-1181">is</w>
     <w lemma="do" pos="vvn" xml:id="A91782-001-a-1190">done</w>
     <pc xml:id="A91782-001-a-1200">,</pc>
    </l>
    <l xml:id="A91782-e230">
     <w lemma="for" pos="acp" xml:id="A91782-001-a-1210">For</w>
     <w lemma="all" pos="d" xml:id="A91782-001-a-1220">all</w>
     <w lemma="earth" pos="ng1" xml:id="A91782-001-a-1230">Earth's</w>
     <w lemma="potentate" pos="n2" xml:id="A91782-001-a-1240">Potentates</w>
     <w join="right" lemma="to" pos="prt" xml:id="A91782-001-a-1250">t'</w>
     <w join="left" lemma="admire" pos="vvi" xml:id="A91782-001-a-1251">dmire</w>
     <pc xml:id="A91782-001-a-1260">,</pc>
     <w lemma="and" pos="cc" xml:id="A91782-001-a-1270">and</w>
     <w lemma="see" pos="vvi" xml:id="A91782-001-a-1280">see</w>
    </l>
    <l xml:id="A91782-e240">
     <w lemma="king" pos="n1" xml:id="A91782-001-a-1290">KING</w>
     <hi xml:id="A91782-e250">
      <w lemma="Charles" pos="nn1" xml:id="A91782-001-a-1300">Charles</w>
     </hi>
     <w lemma="the" pos="d" xml:id="A91782-001-a-1310">the</w>
     <w lemma="second" pos="ord" xml:id="A91782-001-a-1320">Seconds</w>
     <w lemma="christian" pos="jnn" xml:id="A91782-001-a-1330">Christian</w>
     <w lemma="charity" pos="n1" xml:id="A91782-001-a-1340">Charity</w>
     <pc xml:id="A91782-001-a-1350">;</pc>
    </l>
    <l xml:id="A91782-e260">
     <w lemma="witness" pos="vvb" reg="Witness" xml:id="A91782-001-a-1360">Witnesse</w>
     <w lemma="God" pos="nng1" reg="God's" xml:id="A91782-001-a-1370">Gods</w>
     <w lemma="hand" pos="n1" xml:id="A91782-001-a-1380">Hand</w>
     <pc xml:id="A91782-001-a-1390">;</pc>
     <w lemma="heaven" pos="n1" reg="heaven" xml:id="A91782-001-a-1400">Heav'n</w>
     <w lemma="fight" pos="vvz" xml:id="A91782-001-a-1410">fights</w>
     <w lemma="for" pos="acp" xml:id="A91782-001-a-1420">for</w>
     <w lemma="he" pos="pno" xml:id="A91782-001-a-1430">him</w>
     <pc xml:id="A91782-001-a-1440">,</pc>
     <w lemma="by" pos="acp" xml:id="A91782-001-a-1450">by</w>
     <w lemma="good" pos="j" xml:id="A91782-001-a-1460">good</w>
    </l>
    <l xml:id="A91782-e270">
     <w lemma="and" pos="cc" xml:id="A91782-001-a-1470">And</w>
     <w lemma="best" pos="js" xml:id="A91782-001-a-1480">best</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-1490">of</w>
     <w lemma="subject" pos="n2" xml:id="A91782-001-a-1500">Subjects</w>
     <pc xml:id="A91782-001-a-1510">;</pc>
     <w lemma="shed" pos="vvg" xml:id="A91782-001-a-1520">shedding</w>
     <w lemma="no" pos="dx" xml:id="A91782-001-a-1530">no</w>
     <w lemma="man" pos="ng1" reg="man's" xml:id="A91782-001-a-1540">mans</w>
     <w lemma="blood" pos="n1" xml:id="A91782-001-a-1550">Blood</w>
     <pc unit="sentence" xml:id="A91782-001-a-1560">.</pc>
    </l>
    <l xml:id="A91782-e280">
     <w lemma="o" pos="uh" xml:id="A91782-001-a-1570">O</w>
     <w lemma="beyond" pos="acp" xml:id="A91782-001-a-1580">beyond</w>
     <w lemma="thought" pos="n1" xml:id="A91782-001-a-1590">thought</w>
     <pc xml:id="A91782-001-a-1600">!</pc>
     <w lemma="bless" pos="j-vn" reg="blessed" xml:id="A91782-001-a-1610">blest</w>
     <w lemma="comfort" pos="n1" xml:id="A91782-001-a-1620">comfort</w>
     <w lemma="to" pos="acp" xml:id="A91782-001-a-1630">to</w>
     <w lemma="we" pos="pno" xml:id="A91782-001-a-1640">us</w>
     <w lemma="all" pos="d" xml:id="A91782-001-a-1650">all</w>
    </l>
    <l xml:id="A91782-e290">
     <w lemma="send" pos="vvn" xml:id="A91782-001-a-1660">Sent</w>
     <w lemma="by" pos="acp" xml:id="A91782-001-a-1670">by</w>
     <w lemma="the" pos="d" xml:id="A91782-001-a-1680">the</w>
     <w lemma="mean" pos="n2" xml:id="A91782-001-a-1690">means</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-1700">of</w>
     <w lemma="virtue" pos="ng1" reg="virtue's" xml:id="A91782-001-a-1710">Vertues</w>
     <w lemma="general" pos="n1" xml:id="A91782-001-a-1720">General</w>
     <pc xml:id="A91782-001-a-1730">;</pc>
    </l>
    <l xml:id="A91782-e300">
     <w lemma="no" pos="dx" xml:id="A91782-001-a-1740">No</w>
     <w lemma="fiend" pos="n2" xml:id="A91782-001-a-1750">Fiends</w>
     <w lemma="in" pos="acp" xml:id="A91782-001-a-1760">in</w>
     <w lemma="flesh" pos="n1" xml:id="A91782-001-a-1770">flesh</w>
     <w lemma="can" pos="vmd" xml:id="A91782-001-a-1780">could</w>
     <w lemma="soothe" pos="vvi" reg="soothe" xml:id="A91782-001-a-1790">sooth</w>
     <w lemma="he" pos="pno" xml:id="A91782-001-a-1800">him</w>
     <w lemma="to" pos="prt" xml:id="A91782-001-a-1810">to</w>
     <w lemma="refrain" pos="vvi" xml:id="A91782-001-a-1820">refrain</w>
    </l>
    <l xml:id="A91782-e310">
     <w lemma="obedience" pos="n1" xml:id="A91782-001-a-1830">Obedience</w>
     <pc xml:id="A91782-001-a-1840">,</pc>
     <w lemma="true" pos="j" xml:id="A91782-001-a-1850">true</w>
     <w lemma="love" pos="n1" xml:id="A91782-001-a-1860">love</w>
     <w lemma="to" pos="acp" xml:id="A91782-001-a-1870">to</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-1880">his</w>
     <w lemma="sovereign" pos="n1-j" reg="sovereign" xml:id="A91782-001-a-1890">Soveraign</w>
     <pc unit="sentence" xml:id="A91782-001-a-1900">.</pc>
    </l>
    <l xml:id="A91782-e320">
     <w lemma="a" pos="d" xml:id="A91782-001-a-1910">A</w>
     <w lemma="king" pos="n1" xml:id="A91782-001-a-1920">King</w>
     <pc xml:id="A91782-001-a-1930">,</pc>
     <ref corresp="A91782-330-b" target="A91782-330-b" xml:id="A91782e-330-a"/>
     <note cert="high" corresp="A91782-330-a" place="margin" xml:id="A91782e-330-b" xml:lang="lat">
      <w lemma="rex" pos="fla" xml:id="A91782-001-a-1940">Rex</w>
      <w lemma="serenissimus" pos="j" xml:id="A91782-001-a-1950">serenissimus</w>
      <w lemma="Carolus" pos="nn1" xml:id="A91782-001-a-1960">Carolus</w>
      <w lemma="secundus" pos="fla" xml:id="A91782-001-a-1970">Secundus</w>
      <w lemma="noster" pos="fla" xml:id="A91782-001-a-1980">noster</w>
      <pc xml:id="A91782-001-a-1990">,</pc>
      <w lemma="non" pos="fla" xml:id="A91782-001-a-2000">non</w>
      <w lemma="in" pos="fla" xml:id="A91782-001-a-2010">in</w>
      <w lemma="imperio" pos="fla" xml:id="A91782-001-a-2020">imperio</w>
      <w lemma="tanquam" pos="fla" xml:id="A91782-001-a-2030">tanquam</w>
      <w lemma="in" pos="fla" xml:id="A91782-001-a-2040">in</w>
      <w lemma="virtute" pos="fla" xml:id="A91782-001-a-2050">virtute</w>
      <w lemma="securior" pos="fla" xml:id="A91782-001-a-2060">securior</w>
      <pc unit="sentence" xml:id="A91782-001-a-2070">.</pc>
     </note>
     <w lemma="who" pos="crq" xml:id="A91782-001-a-2080">whose</w>
     <w lemma="thought" pos="n2" xml:id="A91782-001-a-2090">thoughts</w>
     <pc xml:id="A91782-001-a-2100">,</pc>
     <w lemma="think" pos="vvb" xml:id="A91782-001-a-2110">think</w>
     <w lemma="it" pos="pn" xml:id="A91782-001-a-2120">it</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-2130">his</w>
     <w lemma="safe" pos="js" xml:id="A91782-001-a-2140">safest</w>
     <w lemma="live" pos="n1-vg" xml:id="A91782-001-a-2150">living</w>
    </l>
    <l xml:id="A91782-e340">
     <w lemma="to" pos="prt" xml:id="A91782-001-a-2160">To</w>
     <w lemma="imitate" pos="vvi" reg="imitate" xml:id="A91782-001-a-2170">immitate</w>
     <w lemma="our" pos="po" xml:id="A91782-001-a-2180">our</w>
     <w lemma="saviour" pos="n1" xml:id="A91782-001-a-2190">Saviour</w>
     <w lemma="in" pos="acp" xml:id="A91782-001-a-2200">in</w>
     <w lemma="forgive" pos="n1-vg" xml:id="A91782-001-a-2210">forgiving</w>
     <pc xml:id="A91782-001-a-2220">;</pc>
    </l>
    <l xml:id="A91782-e350">
     <w lemma="pray" pos="vvg" xml:id="A91782-001-a-2230">Praying</w>
     <w lemma="for" pos="acp" xml:id="A91782-001-a-2240">for</w>
     <w lemma="foe" pos="n2" xml:id="A91782-001-a-2250">Foes</w>
     <pc xml:id="A91782-001-a-2260">,</pc>
     <w lemma="wherein" pos="crq" xml:id="A91782-001-a-2270">wherein</w>
     <w lemma="he" pos="pns" xml:id="A91782-001-a-2280">He</w>
     <w lemma="do" pos="vvz" reg="does" xml:id="A91782-001-a-2290">dos</w>
     <w lemma="comprise" pos="vvi" reg="comprise" xml:id="A91782-001-a-2300">comprize</w>
    </l>
    <l xml:id="A91782-e360">
     <w lemma="the" pos="d" xml:id="A91782-001-a-2310">The</w>
     <w lemma="funeral" pos="n1" xml:id="A91782-001-a-2320">Funeral</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-2330">of</w>
     <w lemma="all" pos="d" xml:id="A91782-001-a-2340">All</w>
     <w lemma="his" pos="po" xml:id="A91782-001-a-2350">his</w>
     <w lemma="injury" pos="n2" xml:id="A91782-001-a-2360">Injuries</w>
     <pc xml:id="A91782-001-a-2370">:</pc>
    </l>
    <l xml:id="A91782-e370">
     <w lemma="this" pos="d" xml:id="A91782-001-a-2380">This</w>
     <w lemma="from" pos="acp" xml:id="A91782-001-a-2390">from</w>
     <w lemma="sad" pos="j" xml:id="A91782-001-a-2400">sad</w>
     <w lemma="exile" pos="n1" xml:id="A91782-001-a-2410">Exile</w>
     <pc xml:id="A91782-001-a-2420">,</pc>
     <w lemma="send" pos="vvd" xml:id="A91782-001-a-2430">sent</w>
     <w lemma="he" pos="pno" xml:id="A91782-001-a-2440">him</w>
     <w lemma="home" pos="av-n" xml:id="A91782-001-a-2450">Home</w>
     <w lemma="to" pos="acp" xml:id="A91782-001-a-2460">to</w>
     <w lemma="Heale" pos="nn1" xml:id="A91782-001-a-2470">Heale</w>
    </l>
    <l xml:id="A91782-e380">
     <w lemma="the" pos="d" xml:id="A91782-001-a-2480">The</w>
     <w lemma="bloody" pos="j" xml:id="A91782-001-a-2490">Bloody</w>
     <w lemma="wound" pos="n2" xml:id="A91782-001-a-2500">Wounds</w>
     <w lemma="of" pos="acp" xml:id="A91782-001-a-2510">of</w>
     <hi xml:id="A91782-e390">
      <w lemma="England" pos="nng1" reg="England's" xml:id="A91782-001-a-2520">Englands</w>
     </hi>
     <w lemma="commonweal" pos="n1" reg="commonweal" xml:id="A91782-001-a-2530">Commonweale</w>
     <pc xml:id="A91782-001-a-2540">:</pc>
    </l>
    <l xml:id="A91782-e400">
     <w lemma="like" pos="acp" xml:id="A91782-001-a-2550">Like</w>
     <w lemma="man" pos="n1" xml:id="A91782-001-a-2560">Man</w>
     <w lemma="and" pos="cc" xml:id="A91782-001-a-2570">and</w>
     <w lemma="wife" pos="n1" xml:id="A91782-001-a-2580">Wife</w>
     <pc xml:id="A91782-001-a-2590">,</pc>
     <w lemma="where" pos="crq" xml:id="A91782-001-a-2600">where</w>
     <w lemma="both" pos="av-d" xml:id="A91782-001-a-2610">both</w>
     <w lemma="in" pos="acp" xml:id="A91782-001-a-2620">in</w>
     <w lemma="love" pos="n1" xml:id="A91782-001-a-2630">Love</w>
     <w lemma="agree" pos="vvi" xml:id="A91782-001-a-2640">agree</w>
     <pc xml:id="A91782-001-a-2650">,</pc>
    </l>
    <l xml:id="A91782-e410">
     <w lemma="king" pos="ng1" xml:id="A91782-001-a-2660">King's</w>
     <w lemma="live" pos="vvb" xml:id="A91782-001-a-2670">live</w>
     <w lemma="in" pos="acp" xml:id="A91782-001-a-2680">in</w>
     <w lemma="peace" pos="n1" xml:id="A91782-001-a-2690">peace</w>
     <pc xml:id="A91782-001-a-2700">,</pc>
     <w lemma="prudent" pos="j" xml:id="A91782-001-a-2710">prudent</w>
     <w lemma="parl'ament" pos="n2" xml:id="A91782-001-a-2720">Parl'aments</w>
     <w lemma="free" pos="j" xml:id="A91782-001-a-2730">Free</w>
     <pc unit="sentence" xml:id="A91782-001-a-2740">.</pc>
    </l>
    <closer xml:id="A91782-e420">
     <signed xml:id="A91782-e430">
      <w lemma="Nathaniel" pos="nn1" xml:id="A91782-001-a-2750">Nathaniel</w>
      <w lemma="Richards" pos="nn1" xml:id="A91782-001-a-2760">Richards</w>
      <pc unit="sentence" xml:id="A91782-001-a-2770">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A91782-e440">
   <div type="colophon" xml:id="A91782-e450">
    <p xml:id="A91782-e460">
     <w lemma="london" pos="nn1" xml:id="A91782-001-a-2780">London</w>
     <pc xml:id="A91782-001-a-2790">,</pc>
     <w lemma="print" pos="vvn" xml:id="A91782-001-a-2800">Printed</w>
     <w lemma="for" pos="acp" xml:id="A91782-001-a-2810">for</w>
     <hi xml:id="A91782-e470">
      <w lemma="j." pos="ab" xml:id="A91782-001-a-2820">J.</w>
      <w lemma="g." pos="ab" xml:id="A91782-001-a-2830">G.</w>
     </hi>
     <w lemma="1660." pos="crd" xml:id="A91782-001-a-2840">1660.</w>
     <pc unit="sentence" xml:id="A91782-001-a-2850"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
